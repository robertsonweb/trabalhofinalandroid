package com.trabalhos.taxiapp;


import com.trabalhos.taxiapp.repositorio.RepositorioUsuario;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Cadastro_de_usuarios extends Activity {
	
	EditText campo_nome, campo_email, campo_senha, campo_telefone;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cadastro_de_usuarios);
		
		campo_nome = (EditText) findViewById(R.id.editText2);
		campo_email = (EditText) findViewById(R.id.editText3);
		campo_senha = (EditText) findViewById(R.id.editText1);
		campo_telefone = (EditText) findViewById(R.id.editText4);
	}
	    
	public void CadastrarClick(View v){
		
		String senha = campo_senha.getText().toString(); 
		String nome = campo_nome.getText().toString(); 
		String email = campo_email.getText().toString(); 
		String telefone = campo_nome.getText().toString(); 
		RepositorioUsuario repoUsr = new RepositorioUsuario();
		if(repoUsr.cadastrarUsuario(email, senha, nome, telefone)){
			Toast.makeText(getApplicationContext(), "Cadastrado com sucresso!", Toast.LENGTH_SHORT).show();
			finish();
		} else {
			Toast.makeText(getApplicationContext(), "Erro ao cadastrar, tente novamente!", Toast.LENGTH_SHORT).show();
		}
		
    	// TERMINAR !!!!!!
    }

}
