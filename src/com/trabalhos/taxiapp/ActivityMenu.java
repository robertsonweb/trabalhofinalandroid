package com.trabalhos.taxiapp;

import com.trabalhos.taxiapp.bean.Usuario;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.app.Activity; 
import android.content.Intent;

public class ActivityMenu extends Activity {

	private Usuario usuario;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		Intent i = getIntent();
		
		if(i.hasExtra("usuario")){
			usuario = (Usuario) i.getExtras().get("usuario");
		} else {
			Toast.makeText(getApplicationContext(), "Não logado!", Toast.LENGTH_SHORT).show();
		}
		
	}

	public void listar(View v){ 
		Intent i = new Intent(this, ActivityListTaxi.class);	
		i.putExtra("usuario", usuario);
		startActivity(i);
		
	}
	
	public void mapa(View v){ 
		Intent i = new Intent(this, ActivityMapa.class);
		i.putExtra("usuario", usuario);
		startActivity(i);		
	}
	
	public void pedidos(View v){
		
	}
}
