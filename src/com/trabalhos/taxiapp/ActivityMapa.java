package com.trabalhos.taxiapp;

import java.util.ArrayList;
import java.util.List;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem; 
import com.trabalhos.taxiapp.bean.Taxi;
import com.trabalhos.taxiapp.overlay.MyItemizedOverlay;
import com.trabalhos.taxiapp.repositorio.RepositorioTaxi;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle; 
import android.view.View;

public class ActivityMapa extends MapActivity implements LocationListener {

	private MapView mapa;
	private MapController mapController; 
	private GeoPoint myGeo;
	
	private int metros = 2;
	private int milisegundos = 2000;
	private LocationManager locMgr;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		mapa = (MapView) findViewById(R.id.mapa);
		
		mapController = mapa.getController();
		mapa.setBuiltInZoomControls(true);
		
		mapController.setZoom(12);
		MyLocationOverlay myLoc = new MyLocationOverlay(this, mapa);		
		
		locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
		locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				milisegundos, metros, this);
		
		List<Overlay> mapOverlays = mapa.getOverlays();
        Drawable drawable = this.getResources().getDrawable(R.drawable.ic_launcher);
        
        MyItemizedOverlay itemizedoverlay = new MyItemizedOverlay(drawable, this);
        
        
        
        RepositorioTaxi taxi = new RepositorioTaxi();
		ArrayList<Taxi> taxis = taxi.getLivres(); 
		
		for (int i = 0; i < taxis.size(); i++) {
			
			OverlayItem overlayitem = new OverlayItem(taxis.get(i).getGeoPoint(), getString(R.string.taxi_descricao), 
					taxis.get(i).getVeiculo());
			itemizedoverlay.addOverlay(overlayitem);
		}
		
		mapOverlays.add(itemizedoverlay);
		mapController.setCenter(taxis.get(taxis.size()-1).getGeoPoint());
		myLoc.enableMyLocation();
        mapOverlays.add(myLoc);
	}
	
	public void atualizar(View v){
		MyLocationOverlay myLoc = new MyLocationOverlay(this, mapa);	
		List<Overlay> mapOverlays = mapa.getOverlays();
        Drawable drawable = this.getResources().getDrawable(R.drawable.ic_launcher);
		MyItemizedOverlay itemizedoverlay = new MyItemizedOverlay(drawable, this);
		RepositorioTaxi taxi = new RepositorioTaxi();
		ArrayList<Taxi> taxis = taxi.getLivres(); 
		mapOverlays.clear();
		for (int i = 0; i < taxis.size(); i++) {
			
			OverlayItem overlayitem = new OverlayItem(taxis.get(i).getGeoPoint(), getString(R.string.taxi_descricao), 
					taxis.get(i).getVeiculo());
			itemizedoverlay.addOverlay(overlayitem);
		}
		mapOverlays.add(itemizedoverlay);
		mapController.setCenter(taxis.get(taxis.size()-1).getGeoPoint());
		myLoc.enableMyLocation();
        mapOverlays.add(myLoc);
	}

	
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		myGeo = new GeoPoint((int)(arg0.getLatitude()*1E6), (int)(arg0.getLongitude()*1E6));		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
 

}
