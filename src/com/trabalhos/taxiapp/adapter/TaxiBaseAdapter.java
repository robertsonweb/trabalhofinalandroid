package com.trabalhos.taxiapp.adapter;

import java.util.ArrayList; 

import com.trabalhos.taxiapp.R;
import com.trabalhos.taxiapp.bean.Taxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;  
import android.widget.TextView;

public class TaxiBaseAdapter extends BaseAdapter {

	
	
	private static ArrayList<Taxi> itemList;
 	private LayoutInflater l_Inflater;
 	private ViewHolder holder;
	
	public TaxiBaseAdapter(Context context, ArrayList<Taxi> itens){
		itemList = itens;
		l_Inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return itemList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return itemList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) { 
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		if (convertView == null) {
			   convertView = l_Inflater.inflate(R.layout.taxi_listview_item, null);
			   holder = new ViewHolder();  
			   holder.imgRight = (ImageView) convertView.findViewById(R.id.itemTxtImage);
			   holder.txtName = (TextView) convertView.findViewById(R.id.itemTxtTitulo);
			   holder.txtDescription = (TextView) convertView.findViewById(R.id.itemTxtDescricao);
			   convertView.setTag(holder);
			  } else {
			   holder = (ViewHolder) convertView.getTag();
			  }
			  Taxi item = itemList.get(position); 
			  holder.imgRight.setImageResource(R.drawable.ic_launcher);	
			  holder.txtName.setText(item.getTaxista());
			  holder.txtDescription.setText(item.getVeiculo());			  
			  return convertView;
	}
	
	class ViewHolder { 
		 ImageView imgRight;
		 TextView  txtName;
		 TextView  txtDescription;
	 }

}
