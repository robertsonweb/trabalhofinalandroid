package com.trabalhos.taxiapp;

import java.util.ArrayList;

import com.google.android.maps.GeoPoint;
import com.trabalhos.taxiapp.adapter.TaxiBaseAdapter;
import com.trabalhos.taxiapp.bean.Taxi;
import com.trabalhos.taxiapp.bean.Usuario;
import com.trabalhos.taxiapp.repositorio.RepositorioTaxi;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.app.Activity; 
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

public class ActivityListTaxi extends Activity implements LocationListener{

	private ListView listViewTaxis;
	private Usuario  user;
	private GeoPoint myGeo;
	
	private int metros = 2;
	private int milisegundos = 2000;
	private LocationManager locMgr;
	
	ArrayList<Taxi> taxis;
	private Usuario usuario;
	private Taxi taxi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_taxi);
		
		Intent i = getIntent();
		
		if(i.hasExtra("usuario")){
			usuario = (Usuario) i.getExtras().get("usuario");
		} else {
			Toast.makeText(getApplicationContext(), "Não logado!", Toast.LENGTH_SHORT).show();
		}
		
		
		findViews();
		fillTaxi();
		
		locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
		locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				milisegundos, metros, this);
		
		listViewTaxis.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				taxi = taxis.get(arg2);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(  
						ActivityListTaxi.this);  
		  
		            alertDialogBuilder.setTitle("Titulo");  
		  
		            alertDialogBuilder  
		                .setMessage("Deseja chamar esse taxi?")  
		                .setCancelable(false)  
		                .setPositiveButton("Sim",new DialogInterface.OnClickListener() {  
		                    public void onClick(DialogInterface dialog,int id) {   
		                    	chamarTaxi();
		                    	dialog.cancel();  
		                    }  
		                  })  
		                .setNegativeButton("Não",new DialogInterface.OnClickListener() {  
		                    public void onClick(DialogInterface dialog,int id) {   
		                        dialog.cancel();  
		                    }  
		                });  
		  
		                AlertDialog alertDialog = alertDialogBuilder.create();  
		  
		                alertDialog.show();  
				
			}
		});
	}
	
	public void chamarTaxi(){
		if(myGeo != null){
			RepositorioTaxi repoTaxi = new RepositorioTaxi();
			if(repoTaxi.solicitarTaxi(usuario.getPk(), taxi.getPk(), myGeo.getLatitudeE6()/1E6, myGeo.getLongitudeE6()/1E6)){
				Toast.makeText(getApplicationContext(), "Taxi chamado!", Toast.LENGTH_SHORT).show();
				ActivityListTaxi.this.finish();
			} else {
				Toast.makeText(getApplicationContext(), "Erro ao chamar taxi. Tente novamente!", Toast.LENGTH_SHORT).show();				
			}
		}else {
			Toast.makeText(this, "Aguarde as suas coordenadas!", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public void fillTaxi(){
		RepositorioTaxi taxi = new RepositorioTaxi();
		taxis = taxi.getLivres();
		TaxiBaseAdapter taxiAdapter = new TaxiBaseAdapter(this, taxis);
		listViewTaxis.setAdapter(taxiAdapter);
	}
	
	public void findViews(){
		listViewTaxis = (ListView) findViewById(R.id.listViewTaxis);
	}

	@Override
	public void onLocationChanged(Location location) {
		myGeo = new GeoPoint((int)(location.getLatitude()*1E6), (int)(location.getLongitude()*1E6));		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
