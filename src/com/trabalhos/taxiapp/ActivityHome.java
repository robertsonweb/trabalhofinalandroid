package com.trabalhos.taxiapp; 
 

import com.trabalhos.taxiapp.bean.Usuario;
import com.trabalhos.taxiapp.repositorio.RepositorioUsuario;
 
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;  
import android.content.Intent;

public class ActivityHome extends Activity {

	EditText edtLogin;
	EditText edtSenha;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home); 	
		edtLogin = (EditText) findViewById(R.id.edtLogin);
		edtSenha = (EditText) findViewById(R.id.edtSenha);
	} 
	
	public void login(View v){
		RepositorioUsuario usr = new RepositorioUsuario();
		
		Usuario user = usr.login(edtLogin.getText().toString(), edtSenha.getText().toString());
		if (user != null) {
			Intent i = new Intent(this, ActivityMenu.class);
			i.putExtra("usuario", user);			
			startActivity(i);
			//Toast.makeText(getApplicationContext(), user.toString(), Toast.LENGTH_SHORT).show();
		} else {
			
			Intent i = new Intent(this, ActivityMenu.class);
			i.putExtra("usuario", user);			
			startActivity(i);
			Toast.makeText(getApplicationContext(), "Dados inválidos", Toast.LENGTH_SHORT).show();
		}
		 
	}
	public void cadastrar(View v){
		Intent i = new Intent(this, Cadastro_de_usuarios.class);
		startActivity(i);
	}
}
