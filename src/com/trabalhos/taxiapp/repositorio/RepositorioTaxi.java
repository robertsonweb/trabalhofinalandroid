package com.trabalhos.taxiapp.repositorio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trabalhos.taxiapp.bean.Taxi;
import com.trabalhos.taxiapp.bean.Usuario;

public class RepositorioTaxi {
	
	JSONHelper jsonHelper = new JSONHelper();	 
	private final String URL_TAXI_LIVRES = "http://trab.ersistemas.info/taxi/livres"; 
	private final String URL_TAXI_SOLICITAR = "http://trab.ersistemas.info/usuario/solicitacao/criar";
	
	public RepositorioTaxi(){
		
	}
	
	public ArrayList<Taxi> getLivres(){
		ArrayList<Taxi> taxis = null;
		JSONHelper jsonHelper = new JSONHelper();
		
		String json = jsonHelper.readJson(URL_TAXI_LIVRES, null);
		
		try {
			JSONArray obj = new JSONArray(json);
			taxis = new ArrayList<Taxi>();
			for (int i = 0; i < obj.length(); i++) {
				Taxi taxi = new Taxi();
				taxi.setPk((Integer) obj.getJSONObject(i).get("pk"));
				JSONObject fields = (JSONObject) obj.getJSONObject(i).get("fields");
				taxi.setLatitude(fields.getDouble("latitude"));
				taxi.setLongitude(fields.getDouble("longitude"));
				taxi.setSituacao(fields.getString("situacao"));
				taxi.setVeiculo(fields.getString("veiculo"));
				taxis.add(taxi);				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return taxis;
	}
	
	public boolean solicitarTaxi(int usuario_id, int taxi_id, double latitude, double longitude) { 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost post = new HttpPost(URL_TAXI_SOLICITAR);

		try {
			StringBuilder builder = new StringBuilder();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("usuario_id",String.valueOf(usuario_id)));
			nameValuePairs.add(new BasicNameValuePair("taxi_id",String.valueOf(taxi_id)));
			nameValuePairs.add(new BasicNameValuePair("latitude",String.valueOf(latitude)));
			nameValuePairs.add(new BasicNameValuePair("longitude",String.valueOf(longitude)));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(post); 			
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				try {
					JSONObject obj = new JSONObject(builder.toString());
					String resultado = obj.getString("resultado");
					if(resultado.equals("OK")){
						return true;
					}					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else { 
			}
		}  catch (IOException e) {
			// TODO Auto-generated catch block
		}
		return false;
	}

}
