package com.trabalhos.taxiapp.repositorio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader; 
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine; 
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
import com.trabalhos.taxiapp.bean.Taxi;
import com.trabalhos.taxiapp.bean.Usuario;
 
public class RepositorioUsuario {

	JSONHelper jsonHelper = new JSONHelper();
	private final String URL_LOGIN = "http://trab.ersistemas.info/usuario/login";
	private final String URL_BUSCAR_USR = "http://trab.ersistemas.info/usuario/buscar/";
	private final String URL_NOVO_USR = "http://trab.ersistemas.info/usuario/criar";
	
	JSONHelper json = new JSONHelper();
	
	public RepositorioUsuario() {


	}
	
	public boolean cadastrarUsuario(String email, String senha, String nome, String telefone) { 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost post = new HttpPost(URL_NOVO_USR);

		try {
			StringBuilder builder = new StringBuilder();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("email",email));
			nameValuePairs.add(new BasicNameValuePair("senha",senha));
			nameValuePairs.add(new BasicNameValuePair("nome",nome));
			nameValuePairs.add(new BasicNameValuePair("telefone",telefone));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(post); 			
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				try {
					JSONObject obj = new JSONObject(builder.toString());
					String resultado = obj.getString("resultado");
					if(resultado.equals("OK")){						
						return true;
					}					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else { 
			}
		}  catch (IOException e) {
			// TODO Auto-generated catch block
		}
		return false;
	}

	public Usuario login(String email, String senha) { 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost post = new HttpPost(URL_LOGIN);

		try {
			StringBuilder builder = new StringBuilder();
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("email",email));
			nameValuePairs.add(new BasicNameValuePair("senha",senha));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(post); 			
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				try {
					JSONObject obj = new JSONObject(builder.toString());
					String resultado = obj.getString("resultado");
					if(resultado.equals("OK")){
						int id = obj.getInt("id");
						return getUsuario(id);
					}					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else { 
			}
		}  catch (IOException e) {
			// TODO Auto-generated catch block
		}
		return null;
	}
	
	public Usuario getUsuario(int id){
		Usuario usr = null;
		String json_txt = json.readJson(URL_BUSCAR_USR + id, null);
		
		try {
			JSONArray array = new JSONArray(json_txt);
			JSONObject obj = array.getJSONObject(0);
			usr = new Usuario();
			for (int i = 0; i < obj.length(); i++) { 
				usr.setPk((Integer) obj.get("pk")); 
				JSONObject fields = (JSONObject) obj.get("fields");
				usr.setNome(fields.getString("nome"));
				usr.setEmail(fields.getString("email"));				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		 
		return usr;
	}

	

}
