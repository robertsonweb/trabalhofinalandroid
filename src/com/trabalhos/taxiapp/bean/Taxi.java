package com.trabalhos.taxiapp.bean;

import java.io.Serializable;

import com.google.android.maps.GeoPoint;

public class Taxi implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int pk;
	private String taxista;
	private double latitude;
	private double longitude;
	private String veiculo;
	private String situacao;
		
	public Taxi(){
		this.pk         = 0;
		this.taxista    = "";
		this.latitude   = 0.0;
		this.longitude  = 0.0;
		this.veiculo 	= "";
		this.situacao   = "";
	}
	
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	
	public String getTaxista() {
		return taxista;
	}

	public void setTaxista(String taxista) {
		this.taxista = taxista;
	}

	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public GeoPoint getGeoPoint(){
		if( (latitude != 0) && (longitude != 0)){
			return new GeoPoint((int)(getLatitude()*1E6), (int)(getLongitude()*1E6) );
		}
		return null;
	}

	@Override
	public String toString() {
		return "Taxi [pk=" + pk + ", taxista=" + taxista + ", latitude="
				+ latitude + ", longitude=" + longitude + ", veiculo="
				+ veiculo + ", situacao=" + situacao + "]";
	}
	
	
	
	
	
}
