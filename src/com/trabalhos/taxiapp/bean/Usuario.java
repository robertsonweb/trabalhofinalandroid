package com.trabalhos.taxiapp.bean;

import java.io.Serializable;

public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pk;
	private String nome;
	private String email;
	public Usuario(int pk, String nome, String email) {
		super();
		this.pk = pk;
		this.nome = nome;
		this.email = email;
	}
	public Usuario() {
		// TODO Auto-generated constructor stub
	}
	public int getPk() {
		return pk;
	}
	public void setPk(int pk) {
		this.pk = pk;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Usuario [pk=" + pk + ", nome=" + nome + ", email=" + email
				+ "]";
	}
	
	
}
